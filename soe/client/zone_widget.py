from enum import Enum
import importlib_resources

from soe.core.game_board import GameBoard

from PySide2.QtWidgets import QPushButton, QLabel
from PySide2.QtGui import QPixmap, QRegion, QColor
from PySide2.QtCore import Signal, Slot


class ZoneState(Enum):
    DISABLED = 0
    UNSELECTED1 = 1
    UNSELECTED2 = 2
    SELECTED_MAIN = 3
    SELECTED_DES = 4


_colors = {
    "disabled": QColor(100, 100, 100),
    "unselected_unhover": QColor(200, 50, 50),
    "unselected1": QColor(200, 50, 50),
    "unselected2": QColor(200, 100, 100),
    "unselected_hover": QColor(230, 80, 80),
    "selected_unhover": QColor(255, 127, 0),
    "selected": QColor(255, 127, 0),
    "selected_hover": QColor(255, 180, 60),
    "destination_state_unhover": QColor(200, 0, 200),
    "destination": QColor(200, 0, 200),
    "destination_state_hover": QColor(230, 0, 230),
}


class ZonStateData:
    base_pixmap = None

    def __init__(self, color):
        self.qcolor = color
        self.unhover_pixmap = self.base_pixmap.copy()
        self.unhover_pixmap.fill(self.qcolor)
        self.hover_pixmap = self.base_pixmap.copy()
        self.hover_pixmap.fill(self.qcolor.lighter(120))


class ZoneWidget(QPushButton):
    """
    Visible object that represents a zone in the game board.
    It allows the user to respond to messages sent by the server.
    _collision_pixmap is the non-visible, interactive part of the widget (applied to the
    button). The other pixmaps are the visible, non-interactive part (applied to the
    label widget).
    """

    zoneActivated = Signal(str)
    zoneSelected = Signal(str)
    clicked_zone = Signal(str)  # This zone's name, as the signal cant use ZoneWidget
    scale = 1
    _faction_info = GameBoard.factions_data

    @property
    def faction(self):
        return self._faction

    @faction.setter
    def faction(self, f):
        if f in self._faction_info.keys():
            self._faction = f
            text = (
                "QLabel { color : rgb("
                + f"{self._faction_info[f][0]},"
                + f"{self._faction_info[f][1]},"
                + f"{self._faction_info[f][2]}"
                + ")}"
            )
            self._number_label.setStyleSheet(text)

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        if v >= 0:
            self._value = v
            self._number_label.setText(str(self._value))
        else:
            raise ValueError

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new_state: ZoneState):
        self._state = new_state
        if self._state == ZoneState.DISABLED:
            self.bgWidget.setPixmap(self._pixmaps[ZoneState.DISABLED].unhover_pixmap)
        elif self._state == ZoneState.UNSELECTED1:
            self.bgWidget.setPixmap(self._pixmaps[ZoneState.UNSELECTED1].unhover_pixmap)
        elif self._state == ZoneState.UNSELECTED2:
            self.bgWidget.setPixmap(self._pixmaps[ZoneState.UNSELECTED2].unhover_pixmap)
        elif self._state == ZoneState.SELECTED_MAIN:
            self.bgWidget.setPixmap(
                self._pixmaps[ZoneState.SELECTED_MAIN].unhover_pixmap
            )
        elif self._state == ZoneState.SELECTED_DES:
            self.bgWidget.setPixmap(
                self._pixmaps[ZoneState.SELECTED_DES].unhover_pixmap
            )

    # @property
    # def text_color(self):
    #     return None

    # @text_color.setter
    # def text_color(self, c):
    #     color_string = c.name()
    #     self._number_label.setStyleSheet(f"QLabel {{ color : {color_string}}}")

    def __init__(self, parent, name, position, size, textPosition):
        super(ZoneWidget, self).__init__(parent)

        self.name = name
        self._state = ZoneState.DISABLED
        self._pixmaps = {}
        self._faction = None
        self._value = 0
        self.clicked.connect(self.clicked_slot)

        # Place zone widget in the right place
        scaledPosition = [n * self.scale for n in position]
        scaledSize = [n * self.scale for n in size]
        scaledTextPosition = [n * self.scale for n in textPosition]
        self.move(*scaledPosition)
        self.setMinimumSize(*scaledSize)

        # Prepare pixmaps
        my_resources = importlib_resources.files("soe.client")
        path_to_data = my_resources / "assets" / "zonesBitmaps" / (name+".png")
        collision_pixmap = QPixmap(str(path_to_data))
        collision_pixmap = collision_pixmap.scaledToWidth(scaledSize[0])
        ZonStateData.base_pixmap = collision_pixmap
        self._pixmaps = {
            ZoneState.DISABLED: ZonStateData(_colors["disabled"]),
            ZoneState.UNSELECTED1: ZonStateData(_colors["unselected1"]),
            ZoneState.UNSELECTED2: ZonStateData(_colors["unselected2"]),
            ZoneState.SELECTED_MAIN: ZonStateData(_colors["selected"]),
            ZoneState.SELECTED_DES: ZonStateData(_colors["destination"]),
        }

        # Prepare background widget for color changes
        # TODO: Use textures instead of colors.
        self.bgWidget = QLabel(self)
        self.bgWidget.setPixmap(self._pixmaps[ZoneState.DISABLED].unhover_pixmap)
        self.bgWidget.setScaledContents(True)

        # Prepare label to show units in the territory
        self._number_label = QLabel(self)
        self._number_label.setText(str(self._value))
        self._number_label.setMask(QRegion())
        self._number_label.move(*scaledTextPosition)

        # Set collision mask to the button
        self.setMask(collision_pixmap.mask())  # THIS DOES THE MAGIC

    def name_value(self):
        return (
            self.name,
            self._value,
        )

    # Events for color changes on hover
    def enterEvent(self, ev):
        # TODO: Profile runtime; if it is too slow the improve logic to avoid
        # unnecesary redraws.
        self.bgWidget.setPixmap(self._pixmaps[self._state].hover_pixmap)

    def leaveEvent(self, ev):
        # TODO: Profile runtime; if it is too slow the improve logic to avoid
        # unnecesary redraws.
        self.bgWidget.setPixmap(self._pixmaps[self._state].unhover_pixmap)

    @Slot()
    def clicked_slot(self):
        if self._state != ZoneState.DISABLED:
            self.clicked_zone.emit(self.name)
