from soe.core.game_board import GameBoard
from soe.core.cards import Card

from PySide2.QtWidgets import QWidget, QGridLayout, QHBoxLayout, QLabel, QPushButton
from PySide2.QtGui import QColor

# from PySide2.QtCore import Qt


class Hand(QWidget):
    def __init__(self, parent=None, player=None, width=300, height=150):
        super(Hand, self).__init__(parent)
        self.player = player
        self._main_layout = QGridLayout()
        self.setLayout(self._main_layout)
        self.setMinimumSize(width, height)
        self._player_name_label = QLabel()
        self._faction_name_label = QLabel()
        self._cards_layout = QHBoxLayout()

        self._main_layout.addLayout(self._cards_layout, 1, 1)
        self._main_layout.addWidget(self._player_name_label, 2, 1)
        self._main_layout.addWidget(self._faction_name_label, 2, 2)

        if player:
            self.set_player_related_widgets(self.player)

    def set_player_related_widgets(self, player):
        self.player = player
        # Change the background color
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(
            self.backgroundRole(),
            QColor(*(GameBoard.factions_data[self.player.faction])),
        )
        self.setPalette(p)

        self._player_name_label.setText(self.player.name)
        self._faction_name_label.setText(self.player.faction)
        # faction_icon_label = QLabel(self.player.faction)
        # self.mainLayout.addWidget(faction_icon_label, 1, 2)

        # Create cards
        # self.CreatePlaceholderCards()

    def _add_card_button(self, card_name):
        self._cards.append(Card(card_name))
        self._cards_layout.addWidget(QPushButton(self._cards[-1].name))

    def _clear_cards(self):
        self._cards = []
        child = self._cards_layout.takeAt(0)
        while child:
            del child
            child = self._cards_layout.takeAt(0)

    def set_hand_info(self, hand: list):
        self._clear_cards()
        for card_name in hand:
            self._add_card_button(card_name)
