"""Widget holding all zone buttons.
The button shapes are taken from localmap.svg, which size is 826 x 567 px.
"""

from pathlib import Path as _Path
from enum import Enum
import importlib_resources

from PySide2.QtWidgets import QWidget, QFrame, QLabel, QPushButton
from PySide2.QtCore import Slot, Signal

# from PySide2.QtGui import QColor

from soe.core.message import (
    Message,
    # PickSingleZone,
    # PickManyZones,
    # PlaceUnits,
    PickedSingleZone,
    PickedManyZones,
    PickedActionZones,
    PlacedUnits,
)

from .zone_widget import ZoneWidget, ZoneState

SVG_MAP_NATIVE_PX_WIDTH = 826
SVG_MAP_NATIVE_PX_HEIGHT = 567
SVG_MAP_CUSTOM_PX_WIDTH = 580
COUNTER_POSITION = (0.05, 0.8)
CONFIRM_POSITION = (0.05, 0.9)


class Mode(Enum):
    NO_MODE = 0
    PICK_SINGLE_ZONE = 1
    PICK_MANY_ZONES = 2
    PICK_ACTION_ZONES = 3
    PLACE_UNITS = 4


class CounterWidget(QLabel):
    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        if v >= 0:
            self._value = v
            self.setText(str(self._value))
        else:
            raise ValueError

    def __init__(self, parent):
        super(CounterWidget, self).__init__(parent)
        self._value = 0
        self.setText(str(self._value))


class MapWidget(QWidget):
    """
    Holds all interactive zones
    """

    map_response = Signal(Message)

    def __init__(self, parent=None):
        super(MapWidget, self).__init__(parent)
        self.scale = SVG_MAP_CUSTOM_PX_WIDTH / SVG_MAP_NATIVE_PX_WIDTH
        self.zones = {}
        self.mode = Mode.NO_MODE
        self._counter = 0
        self._origin = None
        self._destination = None

        self._counter_widget = None
        self._confirm_button = None
        self._create_subwidgets()
        self._set_state_to_all(ZoneState.DISABLED)
        self._selected_zone_name = None

    def _create_zones(self):
        """
        Create zone widgets using data from CSV file.
        """

        # asset_path = _Path(__file__).parent / _Path("assets") / _Path("mapCoords.csv")
        # with open(asset_path, "r") as f:
        with importlib_resources.open_text("soe.client.assets","mapCoords.csv") as f:
            f.readline()
            for line in f:
                (name, x, y, w, h, tx, ty) = line.split(",")
                x = int(x)
                y = int(y)
                w = int(w)
                h = int(h)
                tx = int(tx)
                ty = int(ty)
                ZoneWidget.scale = self.scale
                zone = ZoneWidget(self, name, (x, y), (w, h), (tx, ty))
                zone.clicked_zone.connect(self._zone_clicked)
                # zone.zoneActivated.connect(self.parent.logView.addEntry)
                self.zones[name] = zone

    def _create_background(self):
        """Load and draw the background texture for the map."""
        pass

    def _create_subwidgets(self):
        self._create_zones()
        self._create_background()

        self._counter_widget = CounterWidget(parent=self)
        self._counter_widget.setFrameStyle(QFrame.Panel | QFrame.Raised)
        self._counter_widget.move(
            int(SVG_MAP_NATIVE_PX_WIDTH * COUNTER_POSITION[0] * self.scale),
            int(SVG_MAP_NATIVE_PX_HEIGHT * COUNTER_POSITION[1] * self.scale),
        )
        self._counter_widget.setEnabled(False)
        self._confirm_button = QPushButton("Confirm", parent=self)
        # self._confirm_button.setFrameStyle(QFrame.Panel | QFrame.Raised)
        self._confirm_button.move(
            int(SVG_MAP_NATIVE_PX_WIDTH * CONFIRM_POSITION[0] * self.scale),
            int(SVG_MAP_NATIVE_PX_HEIGHT * CONFIRM_POSITION[1] * self.scale),
        )
        self._confirm_button.clicked.connect(self._emit_response_message)
        self._confirm_button.setEnabled(False)

    def process_PickSingleZone_message(self, message):
        available_zones = message.zones
        self.mode = Mode.PICK_SINGLE_ZONE
        self._set_state(available_zones, ZoneState.UNSELECTED1)
        self._confirm_button.setEnabled(True)

    def process_PickActionZones_message(self, message):
        self._actions = message.actions
        self.mode = Mode.PICK_ACTION_ZONES
        self._set_state([*self._actions.keys()], ZoneState.UNSELECTED1)
        self._confirm_button.setEnabled(True)

    def process_PlaceUnits_message(self, message):
        available_zones = message.zones
        self.mode = Mode.PLACE_UNITS
        self._set_state(available_zones, ZoneState.UNSELECTED1)
        self._counter_widget.setEnabled(True)
        self._counter_widget.value = message.unit_amount
        self._confirm_button.setEnabled(True)

    def _set_state_to_all(self, state: ZoneState):
        for zoneName in self.zones:
            self.zones[zoneName].state = state

    def _set_state(self, zoneList, state: ZoneState):
        for zoneName in zoneList:
            self.zones[zoneName].state = state

    def _get_active_zones(self):
        return [
            z.name
            for z in self.zones.values()
            if (
                (z.state == ZoneState.SELECTED_MAIN)
                or (z.state == ZoneState.SELECTED_DES)
            )
        ]

    def _get_placed_units(self):
        return {
            z.name_value()
            for z in self.zones.values()
            if z.state == ZoneState.SELECTED_MAIN
        }

    def _cleanup_message_processing(self):
        # For PickSingleZone, disable all zones and hide Confirm button.
        # For PlaceUnits, disable all zones and hide counter and confirm.
        # For PickActionZones, disable all zones and hide Confirm button.
        self._set_state_to_all(ZoneState.DISABLED)
        self._origin = None
        self._destination = None
        self._counter_widget.value = 0
        self._counter_widget.setEnabled(False)
        self._confirm_button.setEnabled(False)

    def _emit_response_message(self):
        if self.mode == Mode.NO_MODE:
            return
        elif self.mode == Mode.PICK_SINGLE_ZONE:
            message = PickedSingleZone(None, self._get_active_zones()[0])
        elif self.mode == Mode.PLACE_UNITS:
            message = PlacedUnits(None, self._get_placed_units())
        elif self.mode == Mode.PICK_MANY_ZONES:
            message = PickedManyZones(None, self._get_placed_units())
        elif self.mode == Mode.PICK_ACTION_ZONES:
            message = PickedActionZones(None, (self._origin, self._destination,))
        self.map_response.emit(message)
        self._cleanup_message_processing()

    def set_map(self, map_info):
        for zone_name in map_info.keys():
            self.zones[zone_name].faction = map_info[zone_name][0]
            self.zones[zone_name].value = map_info[zone_name][1]

    @Slot(str)
    def _zone_clicked(self, zone_name):
        if self.mode == Mode.NO_MODE:
            pass  # Shouldn't be necessary
        elif self.mode == Mode.PICK_SINGLE_ZONE:
            # Unset current zone.
            if self._selected_zone_name is not None:
                self.zones[self._selected_zone_name].state = ZoneState.UNSELECTED1
            self._selected_zone_name = zone_name
            self.zones[self._selected_zone_name].state = ZoneState.SELECTED_MAIN
        elif self.mode == Mode.PLACE_UNITS:
            if self._counter_widget.value == 0:
                pass  # Don't do anything, the units are expended.
                # TODO: Allow deselection.
            else:
                self.zones[zone_name].state = ZoneState.SELECTED_MAIN
                self.zones[zone_name].value += 1
                self._counter_widget.value -= 1
        elif self.mode == Mode.PICK_ACTION_ZONES:
            if self._origin is None:
                # Set origin and 2nd selection state
                self._origin = zone_name
                self._set_state_to_all(ZoneState.DISABLED)
                selectables = [self._origin]
                selectables.extend(self._actions[self._origin])
                self._set_state(selectables, ZoneState.UNSELECTED1)
                self.zones[zone_name].state = ZoneState.SELECTED_MAIN

            elif self._origin == zone_name:
                # Clear origin and destination
                self.zones[self._origin].state = ZoneState.UNSELECTED1
                self._origin = None
                if self._destination:
                    if [*self._actions.keys()].count(self._destination) > 0:
                        self.zones[self._destination].state = ZoneState.UNSELECTED1
                    else:
                        self.zones[self._destination].state = ZoneState.DISABLED
                    self._destination = None
                # Set 1st selection state
                self._set_state_to_all(ZoneState.DISABLED)
                self._set_state([*self._actions.keys()], ZoneState.UNSELECTED1)

            else:
                # Set destination and 3rd selection state
                self._set_state_to_all(ZoneState.DISABLED)
                self._destination = zone_name
                self.zones[self._origin].state = ZoneState.SELECTED_MAIN
                self.zones[self._destination].state = ZoneState.SELECTED_DES

    # @Slot(dict)
    # def setZoneValues(self, d):
    #     for z in d:
    #         self.zones[z].value = d[z].value
    #         if d[z].value == 0:
    #             self.zones[z].color = QColor(0, 0, 0)
    #         else:
    #             self.zones[z].color = d[z].faction.qcolor

    # @Slot(str, set)
    # def setMapForPlayer(self, name, zones):
    #     self._set_all_selectable(False)
    #     self.setZones(list(zones))
