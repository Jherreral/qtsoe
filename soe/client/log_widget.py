import logging

from PySide2.QtWidgets import (
    QPlainTextEdit
)
from PySide2.QtCore import Slot


class LogView(logging.Handler):
    def __init__(self, parent=None, width=150, height=150):
        super(LogView, self).__init__()
        self.widget = QPlainTextEdit(parent)
        self.widget.setPlainText("Placeholder log text\n")
        self.widget.resize(width, height)
        self.widget.setReadOnly(True)
        self.widget.setLineWrapMode(QPlainTextEdit.LineWrapMode.WidgetWidth)

    def emit(self, record):
        msg = self.format(record)
        self.widget.appendPlainText(msg)

    def write(self, s):
        pass

    @Slot()
    @Slot(str)
    def addEntry(self, text='blo'):
        self.widget.appendPlainText(text)
        print('appending')
