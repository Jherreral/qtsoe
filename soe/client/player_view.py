import logging

from PySide2.QtWidgets import (
    QWidget,
    QTabWidget,
    QPushButton,
)
from PySide2.QtCore import Signal, Slot

from soe.core.game_board import GameBoard
from soe.core.message import Message, PickedElement

from .hand_widget import Hand
from .log_widget import LogView
from .map_widget import MapWidget
from .track_widget import Track
from .element_dialog import ElementDialog


class PlayerView(QWidget):
    """
    Holds all the widgets for a player client:
    Hand,Track,Log and Map.
    """

    reemit_from_child = Signal(Message)

    def __init__(self, parent=None):
        super(PlayerView, self).__init__(parent)

        self.setMinimumSize(960, 540)

        self.logger = logging.getLogger("PlayerViewLog")
        self.logger.setLevel(10)
        self.logView = LogView(self)
        self.logger.addHandler(self.logView)
        self.logView.widget.move(600, 100)
        self.track = Track(self)
        self.track.move(600, 300)
        self.map_widget = MapWidget(self)
        self.map_widget.map_response.connect(self.reemit_from_child)
        self.hand = Hand(self)
        self.hand.resize(600, 100)
        self.hand.move(0, 400)
        #        self.gameMaster = GameMaster(self)
        #
        #        # Connections from GameMaster to UI elements
        #        self.gameMaster.playersCreated.connect(self.createPlayerHandPanels)
        #        self.gameMaster.playersCreated.connect(self.track.createPlayerColumns)
        #        self.gameMaster.playerMapActionNeeded.connect(self.map.setMapForPlayer)
        #        self.gameMaster.gb.boardChanged.connect(self.map.setZoneValues)
        #        self.gameMaster.gb.trackChanged.connect(self.track.setTableValues)

        # self.hands = QTabWidget(self)
        # self.hands.resize(600, 100)
        # self.hands.move(0, 400)

    def process_PickElements_message(self, message: Message):
        self._dialog = ElementDialog(
            message.element_list, message.element_type, message.to_pick, parent=self
        )
        # TODO: Check if this has to be disconnect upon dialog closure.
        self._dialog.selected_elements.connect(self.reemit_from_dialog)

    def process_UpdateBoard_message(self, message: Message):
        if "map" in message.board_info:
            self.map_widget.set_map(message.board_info["map"])
        if "track" in message.board_info:
            self.track.set_track_info(message.board_info["track"])
        if "hand" in message.board_info:
            self.hand.set_hand_info(message.board_info["hand"][1])

    @Slot(list)
    def reemit_from_dialog(self, picked_elements):
        self.reemit_from_child.emit(PickedElement("Client", picked_elements))
        self._dialog = None

    # @Slot()
    # def createPlayerHandPanels(self, players):
    #     # Should create many player views, not many hand panels...
    #     # ...but this will do for now.
    #     for p in players:
    #         hand = Hand(self, p)
    #         self.hands.addTab(hand, hand.player.name)
