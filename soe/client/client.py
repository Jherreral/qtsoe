from PySide2.QtWidgets import QWidget, QInputDialog
from PySide2.QtCore import Signal, Slot

from soe.core.message import (
    Message,
    PickAmount,
    PickSingleZone,
    PickManyZones,
    PickActionZones,
    PickName,
    PickElements,
    CardNotValid,
    PlaceUnits,
    PickedAmount,
    PickedSingleZone,
    # PickedManyZones,
    PickedActionZones,
    PickedName,
    PickedElement,
    PlacedUnits,
    PlayCard,
    UpdateBoard,
)

from .player_view import PlayerView


class Client(QWidget):

    send_response = Signal(Message)

    def __init__(self, parent=None):
        super(Client, self).__init__(parent)
        self._dialog = None
        self._name = "Client0"
        self.setMinimumSize(640, 480)
        self.move(600, 0)
        self.setWindowTitle(self._name)

        self.player_view = PlayerView(self)
        self.player_view.reemit_from_child.connect(self.send_response)

    @Slot(Message)
    def process_message(self, message: Message):
        print(f"Client: Received {message.type} message.")
        if type(message) == PickAmount:
            self._dialog = QInputDialog()
            try:
                value, ok = self._dialog.getInt(
                    self,
                    "Pick Amount",
                    "Label",
                    minValue=message.min,
                    maxValue=message.max,
                )
                if ok:
                    self.send_response.emit(PickedAmount(self._name, value))
            finally:
                self._dialog = None
        elif type(message) == PickSingleZone:
            self.player_view.map_widget.process_PickSingleZone_message(message)
        elif type(message) == PickActionZones:
            self.player_view.map_widget.process_PickActionZones_message(message)
        elif type(message) == PickName:
            self._dialog = QInputDialog()
            try:
                name, ok = self._dialog.getText(
                    self,
                    f"Player creation ({message.user_number})",
                    "Insert name:",
                    text=f"Player_{message.user_number}",
                )
                if name and ok:
                    self.send_response.emit(PickedName(self._name, name))
            finally:
                self._dialog = None
        elif type(message) == PlaceUnits:
            self.player_view.map_widget.process_PlaceUnits_message(message)
        elif type(message) == PickElements:
            self.player_view.process_PickElements_message(message)
        elif type(message) == UpdateBoard:
            self.player_view.process_UpdateBoard_message(message)
