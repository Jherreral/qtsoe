from PySide2.QtWidgets import QDialogButtonBox, QPushButton, QAbstractButton
from PySide2.QtCore import Signal, Slot, Qt


class ElementDialog(QDialogButtonBox):
    """Dialog with elements to be checked by the user."""

    selected_elements = Signal(list)

    def __init__(self, element_list, element_type, to_pick, parent=None):
        super(ElementDialog, self).__init__(QDialogButtonBox.Ok, parent=parent)
        self.element_list = element_list
        self.element_type = element_type
        self.to_pick = to_pick
        self.picked_amount = 0
        self.element_buttons = []
        self.setOrientation(Qt.Orientation.Vertical)

        if element_type == "String":
            for element in self.element_list:
                new_button = QPushButton(element)
                new_button.setCheckable(True)
                self.element_buttons.append(new_button)
                self.addButton(new_button, QDialogButtonBox.ActionRole)

        self.accepted.connect(self.accept)
        self.clicked.connect(self.click)

        self.show()

    @Slot(QAbstractButton)
    def click(self, button):
        # print(f"Clicked {button.text()}")
        if self.buttonRole(button) == QDialogButtonBox.ActionRole:
            if button.isChecked():
                # print(f"{button.text()} toggled Down")
                self.picked_amount += 1
                if self.picked_amount >= self.to_pick:
                    for b in self.element_buttons:
                        if not b.isChecked():
                            b.setDisabled(True)
            else:
                # print(f"{button.text()} toggled Up")
                self.picked_amount -= 1
                for b in self.element_buttons:
                    b.setEnabled(True)

    @Slot()
    def accept(self):
        # print(f"Picked amount {self.picked_amount}")
        picked_elements = []
        for button in self.element_buttons:
            if button.isChecked():
                # print(f"Picked button {button.text()}")
                picked_elements.append(button.text())
        self.selected_elements.emit(picked_elements)
        self.close()
