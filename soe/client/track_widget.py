from PySide2.QtWidgets import QTableWidget, QTableWidgetItem
from PySide2.QtCore import Slot, QSize


class Track(QTableWidget):
    def __init__(self, parent=None, width=200, height=150):
        super(Track, self).__init__(parent)
        # self.mainLayout = QGridLayout()
        # self.setLayout(self.mainLayout)
        self.setRowCount(4)
        self.setColumnCount(1)
        self.setMinimumSize(width, height)

        self.firstRowItems = [
            QTableWidgetItem("Prod"),
            QTableWidgetItem("Oil"),
            QTableWidgetItem("Sphe"),
            QTableWidgetItem("Caps"),
        ]
        for idx, item in enumerate(self.firstRowItems):
            self.setVerticalHeaderItem(idx, item)

    @Slot(list)
    def createPlayerColumns(self, players):
        self.setColumnCount(len(players))
        for idx, p in enumerate(players):
            self.setColumnWidth(idx, 50)
            header = QTableWidgetItem(p.faction.name[0:5])
            header.setSizeHint(QSize(15, 15))
            self.setHorizontalHeaderItem(idx, header)
            for i in range(4):
                self.setItem(i, idx, QTableWidgetItem("0"))

    @Slot(list)
    def setTableValues(self, data):
        for c, tup in enumerate(data):
            for r, item in enumerate(tup):
                self.setItem(r, c, QTableWidgetItem(str(item)))

    def set_factions_in_header(self, factions_in_play: list):
        self.setColumnCount(len(factions_in_play))
        for idx, faction in enumerate(factions_in_play):
            self.setHorizontalHeaderItem(idx, QTableWidgetItem(faction))

    def set_track_info(self, track_info: list):
        self.setColumnCount(len(track_info))
        for column, data_dict in enumerate(track_info):
            self.setHorizontalHeaderItem(
                column, QTableWidgetItem(str(data_dict["Faction"]))
            )
            self.setItem(0, column, QTableWidgetItem(str(data_dict["Prod"])))
            self.setItem(1, column, QTableWidgetItem(str(data_dict["Oil"])))
            self.setItem(2, column, QTableWidgetItem(str(data_dict["Spheres"])))
            self.setItem(3, column, QTableWidgetItem(str(data_dict["Caps"])))
