from .client import Client as _Client
from .server import Server as _Server

from PySide2.QtWidgets import QApplication


app = QApplication([])

app_server = _Server()
app_client0 = _Client()

app_server.send_message_to_client.connect(app_client0.process_message)
app_client0.send_response.connect(app_server.receive_response)

app_server.show()
app_client0.show()

app.exec_()
