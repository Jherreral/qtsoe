from PySide2.QtCore import Signal, Slot
from PySide2.QtWidgets import QWidget

from .game_session import GameSession
from .master_console import MasterConsole

from soe.core.message import Message, PickAmount, PickedAmount  # noqa E402


class Server(QWidget):
    send_message_to_client = Signal(Message)
    update_client = Signal()

    def __init__(self, parent=None):
        super(Server, self).__init__(parent)
        self._name = "Server - MasterConsole"
        self.setMinimumSize(800, 680)
        self.move(0, 0)
        self.setWindowTitle(self._name)

        self.inbox = []
        self.clients = []
        self.master_console = MasterConsole(self)
        self.master_console.send_console_message.connect(self.send_message)
        self.game_session = GameSession(self, n_players=3)
        self.game_session.send_message.connect(self.send_message_to_client)

        self.game_session.setup_session()

    @Slot(Message)
    def send_message(self, message):
        print(f"Server: Sending {message.type} message.")
        self.send_message_to_client.emit(message)

    @Slot(Message)
    def receive_response(self, message: Message):
        print("Received response from client")
        # self.inbox.append(message)
        # print(f"{len(self.inbox)} messages unread.")
        self.game_session.store_response(message)

    def read_message(self):
        if len(self.inbox) > 0:
            print(self.inbox[-1].value)

    def set_data(self, client, message):
        pass
