"""Game session running in the server."""
from enum import IntFlag, auto
from sys import path as _path

from PySide2.QtCore import QObject, Signal, Slot

from soe.core.game_board import GameBoard
from soe.core.game_components import Player
from soe.core.message import Message, PickName, PickedName


class GameSession(QObject):
    send_message = Signal(Message)

    def __init__(self, parent=None, n_players=None):
        super(GameSession, self).__init__(parent)
        self._board = GameBoard()
        self._n_players = n_players
        self._players = None
        self._ongoing_effects = None
        self._log = None
        self._round = 0
        self._phase = None
        self._actions = None
        self._mobi_order = None
        self._current_player = None
        self._declared_attack = None
        self.response = None
        # self._server_ref = server

    def send_and_get_response(self, message):
        self.send_message.emit(message)
        # After the emit ends, self.response SHOULD be filled.
        return self.response

    @Slot(Message)
    def store_response(self, message):
        self.response = message

    def setup_session(self):
        """Set players, their factions and capitals."""
        self._create_players()
        # self.setInitialForces()
        # self.gb.prepareTurnDeck(True)

    def _create_players(self):
        # Ask each player for their info.
        self._players = [Player()] * self._n_players
        capital_pairs = self._board._map.get_initial_capital_pairs()
        available_names = list(self._board.factions_data.keys())
        for i, pc in enumerate(zip(self._players, capital_pairs)):
            p, caps = pc
            p._user = i + 1
            p._name = self.send_and_get_response(PickName("Server", p._user))
            print(p._name)

        #     faction_name, ok = QInputDialog.getItem(
        #                     self.parent(),
        #                     f'Player creation ({p.user})',
        #                     "Choose faction",
        #                     available_names)
        #     if faction_name and ok:
        #         p.faction = Faction(faction_name, self._board.factions_data[faction_name])
        #         available_names.remove(faction_name)

        #     cap, ok = QInputDialog.getItem(
        #         self.parent(),
        #         f'Player creation ({p.user})',
        #         "Choose capital",
        #         list(caps))
        #     if cap and ok:
        #         p.capital = cap
        #     self._log.info(f'Player {p.name}, {p.faction.name},'
        #                    f'with capital {p.capital}, created.')
        #     print(f'Player {p.name}, {p.faction.name},'
        #           f'with capital {p.capital}, created.')
        # self.playersCreated.emit(self.gb.players)
