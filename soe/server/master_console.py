from PySide2.QtCore import Signal
from PySide2.QtGui import QIntValidator
from PySide2.QtWidgets import (
    QWidget,
    QPushButton,
    QHBoxLayout,
    QVBoxLayout,
    QGridLayout,
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QTabWidget,
    QSizePolicy,
)

from soe.core.message import (
    Message,
    PickAmount,
    PickSingleZone,
    PickManyZones,
    PickActionZones,
    PickName,
    PickElements,
    CardNotValid,
    PlaceUnits,
    PickedAmount,
    PickedSingleZone,
    # PickedManyZones,
    PickedName,
    PickedElement,
    PlacedUnits,
    PlayCard,
)  # noqa E402
from soe.core.game_map import GameMap  # noqa E402
from soe.core.cards import card_names


class MasterConsole(QWidget):
    send_console_message = Signal(Message)

    def __init__(self, parent=None):
        super(MasterConsole, self).__init__(parent)
        self._name = "Console"
        self.zone_list = GameMap.zone_names()
        self.element_list = []

        self._create_server_page()
        self._create_client_page()

        tab_widget = QTabWidget(self)
        tab_widget.addTab(self.server_page, "Server")
        tab_widget.addTab(self.client_page, "Client")
        tab_widget.setMinimumSize(tab_widget.sizeHint())

    def _create_server_page(self):
        self._create_PickAmount_widgets()
        self._create_PickZones_widgets()
        self._create_PickName_widgets()
        self._create_PickElements_widgets()
        self._create_CardNotValid_widgets()

        # Gather all widgets
        left_server_messages_layout = QVBoxLayout()
        left_server_messages_layout.addLayout(self.PickAmount_layout)
        left_server_messages_layout.addLayout(self.PickZones_layout)
        right_server_messages_layout = QVBoxLayout()
        right_server_messages_layout.addLayout(self.PickName_layout)
        right_server_messages_layout.addLayout(self.PickElements_layout)
        right_server_messages_layout.addLayout(self.CardNotValid_layout)
        right_server_messages_layout.addStretch()

        self.server_page = QWidget()
        self.server_page.setMinimumSize(800, 600)
        self.server_page.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        server_messages_layout = QHBoxLayout()
        server_messages_layout.addLayout(left_server_messages_layout)
        server_messages_layout.addLayout(right_server_messages_layout)
        self.server_page.setLayout(server_messages_layout)

    def _create_client_page(self):
        self._create_PickedAmount_widgets()
        self._create_PickedSingleZone_widgets()
        self._create_PlacedUnits_widgets()
        self._create_PickedName_widgets()
        self._create_PickedElement_widgets()
        self._create_PlayCard_widgets()

        # Gather all widgets
        left_client_messages_layout = QVBoxLayout()
        left_client_messages_layout.addLayout(self.PickedAmount_layout)
        left_client_messages_layout.addLayout(self.PickedSingleZone_layout)
        left_client_messages_layout.addLayout(self.PlacedUnits_layout)
        # left_client_messages_layout.addLayout(self.PickedManyZones_layout)
        left_client_messages_layout.addStretch()

        right_client_messages_layout = QVBoxLayout()
        right_client_messages_layout.addLayout(self.PickedName_layout)
        right_client_messages_layout.addLayout(self.PickedElement_layout)
        right_client_messages_layout.addLayout(self.PlayCard_layout)
        right_client_messages_layout.addStretch()

        self.client_page = QWidget()
        self.client_page.setMinimumSize(800, 600)
        self.client_page.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        client_messages_layout = QHBoxLayout()
        client_messages_layout.addLayout(left_client_messages_layout)
        client_messages_layout.addLayout(right_client_messages_layout)
        self.client_page.setLayout(client_messages_layout)

    def _create_PickAmount_widgets(self):
        pick_amount_button = QPushButton("PickAmount")
        pick_amount_button.clicked.connect(self.emit_PickAmount_message)
        min_range_field = QLineEdit()
        min_range_field.setValidator(QIntValidator(bottom=0, top=50))
        max_range_field = QLineEdit()
        max_range_field.setValidator(QIntValidator(bottom=0, top=50))
        self.PickAmount_layout = QHBoxLayout()
        self.PickAmount_layout.addWidget(pick_amount_button)
        self.PickAmount_layout.addWidget(min_range_field)
        self.PickAmount_layout.addWidget(max_range_field)
        self.PickAmount_layout.addStretch()

    def _create_PickZones_widgets(self):
        PickSingleZone_button = QPushButton("PickSingleZone")
        PickSingleZone_button.clicked.connect(self.emit_PickSingleZone_message)
        PickManyZones_button = QPushButton("PickManyZones")
        PickManyZones_button.clicked.connect(self.emit_PickManyZones_message)
        PickActionZones_button = QPushButton("PickActionZones")
        PickActionZones_button.clicked.connect(self.emit_PickActionZones_message)

        PlaceUnits_button = QPushButton("PlaceUnits")
        PlaceUnits_button.clicked.connect(self.emit_PlaceUnits_message)
        self.PlaceUnits_amount_field = QLineEdit()
        self.PlaceUnits_amount_field.setValidator(QIntValidator(bottom=0, top=50))

        PickXZone_buttons_layout = QHBoxLayout()
        PickXZone_buttons_layout.addWidget(PickSingleZone_button)
        PickXZone_buttons_layout.addWidget(PickManyZones_button)
        PickXZone_buttons_layout.addWidget(PickActionZones_button)
        PickXZone_buttons_layout.addWidget(PlaceUnits_button)
        PickXZone_buttons_layout.addWidget(self.PlaceUnits_amount_field)
        PickXZone_buttons_layout.addStretch()

        self.zones_grid_layout = QGridLayout()

        for i, z in enumerate(self.zone_list):
            # Create Qlabel with the name and a checkbox next to it. Then add to a
            # layout for all the zones.
            box = QCheckBox(z)
            self.zones_grid_layout.addWidget(box, i % 20, i / 20)

        self.PickZones_layout = QVBoxLayout()
        self.PickZones_layout.addLayout(PickXZone_buttons_layout)
        self.PickZones_layout.addLayout(self.zones_grid_layout)

    def _create_PickName_widgets(self):
        pick_name_for_user_button = QPushButton("PickName")
        pick_name_for_user_button.clicked.connect(self.emit_PickName_message)
        self.user_number_field = QLineEdit()
        self.user_number_field.setValidator(QIntValidator(bottom=0, top=8))
        self.PickName_layout = QHBoxLayout()
        self.PickName_layout.addWidget(pick_name_for_user_button)
        self.PickName_layout.addWidget(self.user_number_field)
        self.PickName_layout.addStretch()

    def _create_PickElements_widgets(self):
        # Only pick strings by now.
        send_PickElements_button = QPushButton("PickElements")
        send_PickElements_button.clicked.connect(self.emit_PickElements_message)
        self.element_text_field = QLineEdit()
        add_element_button = QPushButton("add")
        add_element_button.clicked.connect(self._add_element_to_list)
        self.element_list_label = QLabel("Elements:")
        clear_list_button = QPushButton("clear")
        clear_list_button.clicked.connect(self._clear_element_list)

        left_layout = QVBoxLayout()
        left_layout.addWidget(send_PickElements_button)
        add_clear_layout = QHBoxLayout()
        add_clear_layout.addWidget(add_element_button)
        add_clear_layout.addWidget(clear_list_button)
        right_layout = QVBoxLayout()
        right_layout.addLayout(add_clear_layout)
        right_layout.addWidget(self.element_text_field)
        right_layout.addWidget(self.element_list_label)
        self.PickElements_layout = QHBoxLayout()
        self.PickElements_layout.addLayout(left_layout)
        self.PickElements_layout.addLayout(right_layout)
        self.PickElements_layout.addStretch()

    def _create_CardNotValid_widgets(self):
        CardNotValid_button = QPushButton("CardNotValid")
        CardNotValid_button.clicked.connect(self.emit_CardNotValid_message)
        CardNotValid_combobox = QComboBox()
        CardNotValid_combobox.addItems(card_names)
        self.CardNotValid_layout = QHBoxLayout()
        self.CardNotValid_layout.addWidget(CardNotValid_button)
        self.CardNotValid_layout.addWidget(CardNotValid_combobox)

    def _create_PickedAmount_widgets(self):
        PickedAmount_button = QPushButton("PickedAmount")
        PickedAmount_button.clicked.connect(self.emit_PickedAmount_message)
        self.PickedAmount_field = QLineEdit()
        self.PickedAmount_field.setValidator(QIntValidator(bottom=0, top=50))
        self.PickedAmount_layout = QHBoxLayout()
        self.PickedAmount_layout.addWidget(PickedAmount_button)
        self.PickedAmount_layout.addWidget(self.PickedAmount_field)
        self.PickedAmount_layout.addStretch()

    def _create_PickedSingleZone_widgets(self):
        PickedSingleZone_button = QPushButton("PickedSingleZone")
        PickedSingleZone_button.clicked.connect(self.emit_PickedSingleZone_message)
        PickedSingleZone_combobox = QComboBox()
        PickedSingleZone_combobox.addItems(self.zone_list)
        self.PickedSingleZone_layout = QHBoxLayout()
        self.PickedSingleZone_layout.addWidget(PickedSingleZone_button)
        self.PickedSingleZone_layout.addWidget(PickedSingleZone_combobox)
        self.PickedSingleZone_layout.addStretch()

    def _create_PlacedUnits_widgets(self):
        PlacedUnits_button = QPushButton("PlacedUnits")
        add_zone_button = QPushButton("add zone")
        clear_zone_button = QPushButton("clear all")
        PlacedUnits_button.clicked.connect(self.emit_PlacedUnits_message)
        add_zone_button.clicked.connect(self._add_zone_widget)
        clear_zone_button.clicked.connect(self._delete_zone_widgets)

        add_clear_layout = QHBoxLayout()
        add_clear_layout.addWidget(add_zone_button)
        add_clear_layout.addWidget(clear_zone_button)
        self.PlacedUnits_zone_widgets_layout = QVBoxLayout()
        right_layout = QVBoxLayout()
        right_layout.addLayout(add_clear_layout)
        right_layout.addLayout(self.PlacedUnits_zone_widgets_layout)
        self.PlacedUnits_layout = QHBoxLayout()
        self.PlacedUnits_layout.addWidget(PlacedUnits_button)
        self.PlacedUnits_layout.addLayout(right_layout)
        self.PlacedUnits_layout.addStretch()

    def _create_PickedName_widgets(self):
        PickedName_button = QPushButton("PickedName")
        PickedName_button.clicked.connect(self.emit_PickedName_message)
        self.PickedName_field = QLineEdit()
        self.PickedName_layout = QHBoxLayout()
        self.PickedName_layout.addWidget(PickedName_button)
        self.PickedName_layout.addWidget(self.PickedName_field)
        self.PickedName_layout.addStretch()

    def _create_PickedElement_widgets(self):
        PickedElement_button = QPushButton("PickedElement")
        PickedElement_button.clicked.connect(self.emit_PickedElement_message)
        self.PickedElement_field = QLineEdit()
        self.PickedElement_layout = QHBoxLayout()
        self.PickedElement_layout.addWidget(PickedElement_button)
        self.PickedElement_layout.addWidget(self.PickedElement_field)
        self.PickedElement_layout.addStretch()

    def _create_PlayCard_widgets(self):
        PlayCard_button = QPushButton("PlayCard")
        PlayCard_button.clicked.connect(self.emit_PlayCard_message)
        PlayCard_combobox = QComboBox()
        PlayCard_combobox.addItems(card_names)
        self.PlayCard_layout = QHBoxLayout()
        self.PlayCard_layout.addWidget(PlayCard_button)
        self.PlayCard_layout.addWidget(PlayCard_combobox)
        self.PlayCard_layout.addStretch()

    def emit_PickAmount_message(self):
        self.send_console_message.emit(
            PickAmount(
                self._name,
                int(self.PickAmount_layout.itemAt(1).widget().text()),
                int(self.PickAmount_layout.itemAt(2).widget().text()),
            )
        )

    def _get_zones_from_grid(self):
        zone_list = []
        checkbox_layout = self.zones_grid_layout
        for i in range(checkbox_layout.count()):
            checkbox = checkbox_layout.itemAt(i).widget()
            if checkbox.isChecked():
                zone_list.append(checkbox.text())
        return zone_list

    def emit_PickSingleZone_message(self):
        zone_list = self._get_zones_from_grid()
        if len(zone_list) == 0:
            print("No zones selected for message.")
        else:
            self.send_console_message.emit(PickSingleZone(self._name, zone_list,))

    def emit_PickManyZones_message(self):
        zone_list = self._get_zones_from_grid()
        if len(zone_list) == 0:
            print("No zones selected for message.")
        else:
            self.send_console_message.emit(PickManyZones(self._name, zone_list))

    def emit_PickActionZones_message(self):
        origins = self._get_zones_from_grid()
        destinations = GameMap.get_destinations(origins)
        action_zones = {origin: destinations[idx] for idx, origin in enumerate(origins)}
        if len(action_zones) == 0:
            print("No zones selected for message.")
        else:
            self.send_console_message.emit(PickActionZones(self._name, action_zones))

    def emit_PlaceUnits_message(self):
        zone_list = self._get_zones_from_grid()
        if len(zone_list) == 0:
            print("No zones selected for message.")
        else:
            self.send_console_message.emit(
                PlaceUnits(
                    self._name, zone_list, int(self.PlaceUnits_amount_field.text())
                )
            )

    def emit_PickName_message(self):
        self.send_console_message.emit(
            PickName(self._name, int(self.user_number_field.text()))
        )

    def emit_PickElements_message(self):
        self.send_console_message.emit(PickElements(self._name, self.element_list))

    def emit_CardNotValid_message(self):
        self.send_console_message.emit(
            CardNotValid(
                self._name, self.CardNotValid_layout.itemAt(1).widget().currentText(),
            )
        )

    def emit_PickedAmount_message(self):
        self.send_console_message.emit(
            PickedAmount(self._name, self.PickedAmount_field.text(),)
        )

    def emit_PickedSingleZone_message(self):
        self.send_console_message.emit(
            PickedSingleZone(
                self._name,
                self.PickedSingleZone_layout.itemAt(1).widget().currentText(),
            )
        )

    def emit_PlacedUnits_message(self):
        if self.PlacedUnits_zone_widgets_layout.count() == 0:
            print("No zones selected")
        else:
            zone_amount_pairs_list = []
            for i in range(self.PlacedUnits_zone_widgets_layout.count()):
                combobox_field_pair = self.PlacedUnits_zone_widgets_layout.itemAt(
                    0
                ).layout()
                field_item = combobox_field_pair.itemAt(1)
                combobox_item = combobox_field_pair.itemAt(0)
                zone = combobox_item.widget().currentText()
                amount = int(field_item.widget().text())
                zone_amount_pairs_list.append((zone, amount,))
            self.send_console_message.emit(
                PlacedUnits(self._name, zone_amount_pairs_list)
            )

    def emit_PickedName_message(self):
        self.send_console_message.emit(
            PickedName(self._name, self.PickedName_field.text())
        )

    def emit_PickedElement_message(self):
        self.send_console_message.emit(
            PickedElement(self._name, self.PickedElement_field.text())
        )

    def emit_PlayCard_message(self):
        self.send_console_message.emit(
            PlayCard(self._name, self.PlayCard_layout.itemAt(1).widget().currentText(),)
        )

    def _add_element_to_list(self):
        self.element_list.append(self.element_text_field.text())
        print(f"Added {self.element_list[-1]}")
        new_text = ""
        for element in self.element_list:
            new_text += f"{element}\n"
        self.element_list_label.setText(f"Elements:\n{new_text}")

    def _clear_element_list(self):
        self.element_list.clear()
        self.element_list_label.setText("Elements:")

    def _add_zone_widget(self):
        new_layout = QHBoxLayout()
        new_combobox = QComboBox()
        new_combobox.addItems(self.zone_list)
        new_field = QLineEdit()
        new_field.setValidator(QIntValidator(bottom=0, top=50))
        new_layout.addWidget(new_combobox)
        new_layout.addWidget(new_field)
        self.PlacedUnits_zone_widgets_layout.addLayout(new_layout)

    def _delete_zone_widgets(self):
        for i in range(self.PlacedUnits_zone_widgets_layout.count()):
            combobox_field_pair = self.PlacedUnits_zone_widgets_layout.takeAt(
                0
            ).layout()
            field_item = combobox_field_pair.takeAt(1)
            combobox_item = combobox_field_pair.takeAt(0)
            combobox_item.widget().deleteLater()
            field_item.widget().deleteLater()
            combobox_field_pair.layout().deleteLater()
