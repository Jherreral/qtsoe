"""Living, time-dependent elements of the game."""
from enum import IntFlag, auto


class Phase(IntFlag):
    MOBI_START = auto()
    MOBI_ARRANGE_TURN_DECK = auto()
    MOBI_PLACING_UNITS = auto()
    MOBI_END = auto()
    TURN_ACTION = auto()
    TURN_COMBAT_DECLARE_ATTACK = auto()
    TURN_COMBAT_DICE_ROLLED = auto()
    TURN_MOVEMENT = auto()
    TURN_END = auto()


class Player:
    def __init__(self):
        # self._id = None
        # self._user = None
        self.name = None
        self.faction = None
        self.team = None
        self.capital = None
        self.hand = None


class Faction:
    def __init__(self, name, color):
        self.name = name
        self.color = color
        # self.qcolor = QColor.fromRgb(*self.color)


class DeclaredAttack:
    def __init__(self):
        self._attacker = None
        self._defender = None
        self._attacking_zone = None
        self._defending_zone = None
