from more_itertools import first_true

from .game_map import GameMap

# from .game_components import Faction, Phase, Player

# from enum import Enum
# from random import shuffle
# from pathlib import Path
# from json import load as _load


class GameBoard:

    # boardChanged = Signal(dict)
    # trackChanged = Signal(list)
    factions_data = {
        "Internationalism": (166, 237, 237),
        "Dynasty": (2, 219, 75),
        "Teocracy": (98, 46, 153),
        "Communism": (41, 27, 8),
        "Democracy": (30, 105, 235),
        "Caliphate": (94, 61, 17),
        "Fascism": (201, 0, 30),
        "Imperialism": (201, 185, 4),
    }

    def __init__(self, *args):
        super(GameBoard, self).__init__(*args)
        self.mapGraph = []
        self.turn_deck = []
        self._map = GameMap()
        self.special_deck = None
        self.discard_deck = None
        self.factions_in_play = []
        self.players = []
        self._round = 0
        self._phase = None
        # self.factions = [
        #     Faction("Internationalism", (166, 237, 237)),
        #     Faction("Dynasty", (2, 219, 75)),
        #     Faction("Teocracy", (98, 46, 153)),
        #     Faction("Communism", (41, 27, 8)),
        #     Faction("Democracy", (30, 105, 235)),
        #     Faction("Caliphate", (94, 61, 17)),
        #     Faction("Fascism", (201, 0, 30)),
        #     Faction("Imperialism", (201, 185, 4)),
        # ]

    def setBoardZoneValues(self, zvf_list: list):  # Legacy
        for (z, v, f) in zvf_list:
            self.zones[z].value = v
            self.zones[z].faction = f
        self.computeTrackInfo()
        # self.boardChanged.emit(self.zones)
        # self.trackChanged.emit(
        #    [(p.production, p.oil, p.spheres, p.capitals)
        #     for p in self.players])

    def computeTrackInfo(self):  # Legacy
        # Clear all players oil, production and spheres.
        for p in self.players:
            p.oil = 0
            p.production = 0
            p._sphereCount = self.sphereZoneCount
            p.spheres = 0
            p.capitals = 0

        for z in self.zones:
            # Add this zone's oil and production to the proper player,
            # and decrease sphere counter.
            if self.zones[z].faction is not None:
                p = next(x for x in self.players if x.faction == self.zones[z].faction)
                p.oil += self.zones[z].oil
                p.production += self.zones[z].production
                p._sphereCount[self.zones[z].sphere] -= 1
                if self.zones[z].capital:
                    p.capitals += 1
        p.spheres = list(p._sphereCount.values()).count(0)

    def prepareTurnDeck(self, firstTurn):  # Legacy
        assert len(self.turn_deck) == 0
        for p in self.players:
            self.turn_deck.append(p.name)
            self.turn_deck.append(p.name)
            if not firstTurn:
                for oil in range(p.oil):
                    self.turn_deck.append(p.name)

    def getPlayer_N(self, name):  # Legacy
        return next(x for x in self.players if x.name == name)

    def getPlayer_F(self, faction):  # Legacy
        return next(x for x in self.players if x.faction == faction)

    def _get_player_hand_info(self, player_name) -> tuple:
        player = first_true(
            self.players, pred=(lambda player: player.name == player_name),
        )
        return (player.name, player.hand)

    def _get_deck_info(self) -> str:
        return self.discard_deck[0]

    # Game procedures ---------------------------------------
    # Phase logic --------
    def _game_setup(self):
        pass

    def _play_card(self):
        pass

    def _mobi_start(self):
        pass

    def _mobi_arrange_turn_deck(self):
        pass

    def _mobi_place_units(self):
        pass

    def _mobi_end(self):
        pass

    def _turn_action(self):
        pass

    def _turn_combat_declare_attack(self):
        pass

    def _turn_combat_dice_rolled(self):
        pass

    def _turn_movement(self):
        pass

    def _turn_end(self):
        pass
