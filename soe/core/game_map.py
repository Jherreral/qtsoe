"""Map data structure."""

from typing import List
from random import shuffle
from pathlib import Path as _Path
from json import load as _load
import importlib_resources

from more_itertools import first_true


class Zone:
    def __init__(self):
        # Static const data
        self._id = None
        self._name = None
        self._sphere = None
        self._capital = None
        self._production = None
        self._oil = None
        self._interest = None
        self._sea = None
        # Dynamic data
        self.faction = None
        self.units = 0

    def __repr__(self):
        return f"{self._name}, {self.faction}, {self.units}"


class Graph:
    def __init__(self):
        self._graph_data = []
        self._map_connections = {}

    def get_zone_connections(self, zone: str):
        return self._map_connections[zone]


def game_map_init(cls):
    cls._load_from_assets()
    return cls


@game_map_init
class GameMap:
    """Holds map data and utility methods."""

    @classmethod
    def zone_names(cls):
        return [z for z in cls._zone_data]

    @classmethod
    def get_destinations(cls, origins: List[str]):
        return [cls._graph.get_zone_connections(origin) for origin in origins]

    @classmethod
    def _parse_zones(cls):
        resource = importlib_resources.files("soe.core") / "assets" / "SpheresDataBoard.csv"
        with open(resource,"r") as f:
            for line in f.readlines():
                data = line.split(";")
                zone = Zone()
                zone._id = int(data[0])
                zone._name = data[1]
                zone._sphere = int(data[2])
                zone._capital = bool(int(data[6]))
                zone._production = int(data[3])
                zone._oil = int(data[4])
                zone._interest = bool(int(data[5]))
                zone._sea = bool(zone._sphere)
                keyName = data[1]
                cls._zone_data[keyName] = zone

    @classmethod
    def _parse_edges(cls):
        resource = importlib_resources.files("soe.core") / "assets" / "IncidenceT2.txt"
        with open(resource, "r") as f:
            for line in f.readlines():
                nodeA = int(line.find("1"))
                nodeB = int(line.find("1", nodeA + 1))
                cls._graph._graph_data.append((nodeA, nodeB))

    @classmethod
    def _parse_map_connections(cls):
        resource = importlib_resources.files("soe.core") / "assets" / "mapConnections.json"
        with open(resource, "r") as f:
            cls._graph._map_connections = _load(f)

    @classmethod
    def _load_from_assets(cls):
        """
        Load data from assets in disc to RAM,
        creating all the necessary game objects
        """
        # path_to_assets = _Path(__file__).resolve().parent / _Path("assets")
        # if not path_to_assets.exists():
        if not "assets" in importlib_resources.contents("soe.core"):
            raise RuntimeError("Unable to read assets.")
        cls._parse_zones()
        cls._parse_edges()
        cls._parse_map_connections()

    _zone_data = {}
    _graph = Graph()
    _sphere_zone_count = {
        1: 3,
        2: 5,
        3: 5,
        4: 4,
        5: 5,
        6: 3,
        7: 4,
        8: 5,
        9: 4,
        10: 4,
        11: 5,
        12: 3,
        13: 6,
        14: 5,
        15: 4,
        16: 3,
        17: 4,
        18: 3,
    }

    def __init__(self, factions_in_play: list = []):
        self._zones = self._zone_data.copy()
        self.factions_in_play = factions_in_play
        self.modified = False

    def _fill_factions_from_map(self):  # Unused yet.
        factions_set = set()
        for zone in self._zones.values():
            factions_set.add(zone.faction)
        factions_set.remove(None)
        self.factions_in_play = sorted(list(factions_set))

    def get_initial_capital_pairs(self):
        capital_list = [c for c in self._zone_data if self._zone_data[c]._capital]
        if len(capital_list) % 2 == 0:
            shuffle(capital_list)
            return zip(capital_list[::2], capital_list[1::2])

    def list_zones(self, faction: str):
        pass

    def get_player_oil(self, faction: str):  # Unused yet.
        pass

    def get_faction_zones(self, faction: str):  # Unused yet.
        zones = [
            self._zones[z]
            for z in self._zones.keys()
            if self._zones[z].faction == faction
        ]
        return zones

    def get_faction_production(self, faction: str):  # Unused yet.
        pass

    def get_faction_spheres(self, faction: str):
        zones = self.get_faction_zones(faction)
        sphere_current_count = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
            7: 0,
            8: 0,
            9: 0,
            10: 0,
            11: 0,
            12: 0,
            13: 0,
            14: 0,
            15: 0,
            16: 0,
            17: 0,
            18: 0,
        }
        for pz in zones:
            if pz._sphere is not None:
                sphere_current_count[pz._sphere] += 1

        spheres = []
        for key in sphere_current_count.keys():
            if sphere_current_count[key] == self._sphere_zone_count[key]:
                spheres.append(key)
            elif sphere_current_count[key] > self._sphere_zone_count[key]:
                raise ValueError
        return spheres

    def get_player_capitals(self, faction: str):  # Unused yet.
        zones = self.get_faction_zones(faction)
        capitals = filter(zones, lambda x: x._capital)
        return capitals

    def set_game_info(self, map_data: dict) -> None:
        for key in map_data.keys():
            self._zones[key].faction = map_data[key][0]
            self._zones[key].units = map_data[key][1]
        self.modified = True  # Unused yet.

    def get_map_info(self) -> dict:
        data_dict = {}
        for key in self._zones.keys():
            data_dict[key] = (
                self._zones[key].faction,
                self._zones[key].units,
            )
        return data_dict

    def get_track_info(self) -> list:
        if self.factions_in_play:
            track_info = [
                {"Faction": f, "Prod": 0, "Oil": 0, "Spheres": 0, "Caps": 0}
                for f in self.factions_in_play
            ]
            for zone in self._zones.values():
                if zone.faction and zone.units > 0:
                    idx = self.factions_in_play.index(zone.faction)
                    faction_dict = first_true(
                        track_info,
                        pred=(
                            lambda faction_dict: faction_dict["Faction"] == zone.faction
                        ),
                    )
                    faction_dict["Prod"] += zone._production
                    faction_dict["Oil"] += zone._oil
                    faction_dict["Caps"] += 1 if zone._capital else 0

            for idx, faction in enumerate(self.factions_in_play):
                track_info[idx]["Spheres"] = len(self.get_faction_spheres(faction))

            return track_info
