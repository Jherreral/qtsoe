"""Messages for client-server communication."""
from time import time as _time


class Message:
    def __init__(self, emitter):
        self.type = None
        self.emitter = emitter
        self.timestamp = _time()


class PickAmount(Message):
    def __init__(self, emitter, min: int, max: int):
        super(PickAmount, self).__init__(emitter)
        self.type = "PickAmount"
        self.min = min
        self.max = max


class PickedAmount(Message):
    def __init__(self, emitter, value: int):
        super(PickedAmount, self).__init__(emitter)
        self.type = "PickedAmount"
        self.value = value


class PickSingleZone(Message):
    def __init__(self, emitter, zones: list):
        super(PickSingleZone, self).__init__(emitter)
        self.type = "PickSingleZone"
        self.zones = zones


class PickedSingleZone(Message):
    def __init__(self, emitter, zone: str):
        super(PickedSingleZone, self).__init__(emitter)
        self.type = "PickedSingleZone"
        self.zone = zone


class PickManyZones(Message):
    def __init__(self, emitter, zone_list: list):
        super(PickManyZones, self).__init__(emitter)
        self.type = "PickManyZones"
        self.zone_list = zone_list


class PickedManyZones(Message):
    def __init__(self, emitter, zone_list: list):
        super(PickedManyZones, self).__init__(emitter)
        self.type = "PickedManyZone"
        self.zone_list = zone_list


class PickName(Message):
    def __init__(self, emitter, user_number: int):
        super(PickName, self).__init__(emitter)
        self.type = "PickName"
        self.user_number = user_number


class PickedName(Message):
    def __init__(self, emitter, name: str):
        super(PickedName, self).__init__(emitter)
        self.type = "PickedName"
        self.name = name


class PickElements(Message):
    def __init__(self, emitter, element_list: list, element_type, to_pick: int = 1):
        super(PickElements, self).__init__(emitter)
        self.type = "PickElements"
        self.element_list = element_list
        self.element_type = element_type
        self.to_pick = to_pick


class PickedElement(Message):
    def __init__(self, emitter, elements: list):
        super(PickedElement, self).__init__(emitter)
        self.type = "PickedElements"
        self.elements = elements


class PlaceUnits(Message):
    def __init__(self, emitter, zones: list, unit_amount: int):
        super(PlaceUnits, self).__init__(emitter)
        self.type = "PlaceUnits"
        self.zones = zones
        self.unit_amount = unit_amount


class PlacedUnits(Message):
    def __init__(self, emitter, zones_units: set):
        super(PlacedUnits, self).__init__(emitter)
        self.type = "PlacedUnits"
        self.zones_units = zones_units


class PickActionZones(Message):
    def __init__(self, emitter, actions: dict):
        super(PickActionZones, self).__init__(emitter)
        self.type = "PickActionZones"
        self.actions = actions


class PickedActionZones(Message):
    def __init__(self, emitter, action_zones: tuple):
        super(PickedActionZones, self).__init__(emitter)
        self.type = "PickedActionZones"
        self.action_zones = action_zones


class CardNotValid(Message):
    def __init__(self, emitter, rejected_card):
        super(CardNotValid, self).__init__(emitter)
        self.type = "CardNotValid"
        self.rejected_card = rejected_card


class PlayCard(Message):
    def __init__(self, emitter, card_to_play):
        super(PlayCard, self).__init__(emitter)
        self.type = "PlayCard"
        self.card_to_play = card_to_play


class UpdateBoard(Message):
    def __init__(self, emitter, board_info: dict):
        super(UpdateBoard, self).__init__(emitter)
        self.type = "UpdateBoard"
        self.board_info = board_info
