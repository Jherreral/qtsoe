"""Card data.
It is hardcoded due to the fact that its methods and behaviors must be hardcoded.
"""


class Card:
    def __init__(self, name):
        # self._id = None
        self.name = name


card_text_name = [
    "Air supremacy",
    "Anti-Air",
    "Arms race",
    "Bombard",
    "Carrier Support",
    "Cease fire",
    "Charismatic leader",
    "Combat Copter",
    "Conspiracy",
    "Conversion",
    "Convoy",
    "Diplomacy",
    "Draft",
    "Drone",
    "Elite Unit",
    "Energy sector",
    "Heavy armor",
    "ICBM",
    "Infrastructure",
    "Intercept",
    "Maritime power",
    "Nuclear Missile",
    "Oil Boom",
    "Paradrop",
    "Police State",
    "Research",
    "Resupply",
    "Revolution",
    "Rocket fire",
    "Sniper",
    "Spark unrest",
    "Special forces",
    "Sub strike",
    "Surface",
]

card_names = [
    "air_supremacy",
    "anti_air",
    "arms_race",
    "bombard",
    "carrier_support",
    "cease_fire",
    "charismatic_leader",
    "combat_copter",
    "conspiracy",
    "conversion",
    "convoy",
    "diplomacy",
    "draft",
    "drone",
    "elite_unit",
    "energy_sector",
    "heavy_armor",
    "icbm",
    "infrastructure",
    "intercept",
    "maritime_power",
    "nuclear_missile",
    "oil_boom",
    "paradrop",
    "police_state",
    "research",
    "resupply",
    "revolution",
    "rocket_fire",
    "sniper",
    "spark_unrest",
    "special_forces",
    "sub_strike",
    "surface",
]


def air_supremacy():
    pass


def anti_air():
    pass


def arms_race():
    pass


def bombard():
    pass


def carrier_support():
    pass


def cease_fire():
    pass


def charismatic_leader():
    pass


def combat_copter():
    pass


def conspiracy():
    pass


def conversion():
    pass


def convoy():
    pass


def diplomacy():
    pass


def draft():
    pass


def drone():
    pass


def elite_unit():
    pass


def energy_sector():
    pass


def heavy_armor():
    pass


def icbm():
    pass


def infrastructure():
    pass


def intercept():
    pass


def maritime_power():
    pass


def nuclear_missile():
    pass


def oil_boom():
    pass


def paradrop():
    pass


def police_state():
    pass


def research():
    pass


def resupply():
    pass


def revolution():
    pass


def rocket_fire():
    pass


def sniper():
    pass


def spark_unrest():
    pass


def special_forces():
    pass


def sub_strike():
    pass


def surface():
    pass


fx_dict = {
    "air_supremacy": air_supremacy,
    "anti_air": anti_air,
    "arms_race": arms_race,
    "bombard": bombard,
    "carrier_support": carrier_support,
    "cease_fire": cease_fire,
    "charismatic_leader": charismatic_leader,
    "combat_copter": combat_copter,
    "conspiracy": conspiracy,
    "conversion": conversion,
    "convoy": convoy,
    "diplomacy": diplomacy,
    "draft": draft,
    "drone": drone,
    "elite_unit": elite_unit,
    "energy_sector": energy_sector,
    "heavy_armor": heavy_armor,
    "icbm": icbm,
    "infrastructure": infrastructure,
    "intercept": intercept,
    "maritime_power": maritime_power,
    "nuclear_missile": nuclear_missile,
    "oil_boom": oil_boom,
    "paradrop": paradrop,
    "police_state": police_state,
    "research": research,
    "resupply": resupply,
    "revolution": revolution,
    "rocket_fire": rocket_fire,
    "sniper": sniper,
    "spark_unrest": spark_unrest,
    "special_forces": special_forces,
    "sub_strike": sub_strike,
    "surface": surface,
}
