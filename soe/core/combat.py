"""Module with the required functions """
from enum import IntEnum
from random import randint


# Combat data structures ---------------------------------
class CombatDice(IntEnum):
    BLANK = 0
    BLOCK = 1
    KILL = 2


class CommanderState:
    def __init__(self):
        self.dice_kills = 0
        self.modifier_kills = 0
        self.bonus_kills = 0
        self.bonus_blocks = 0
        self.losses = 0


class CombatState:
    def __init__(self):
        self.base_defender_bonus_dice


class PreCombatData:
    def __init__(
        self, attacker: str, defender: str, origin: str, destination: str, units: int
    ):
        self.attacker = attacker
        self.defender = defender
        self.origin = origin
        self.destination = destination
        self.attacker_units = units
        # Not sure if should be sure, but it should be somewhere.
        self.attacker_bonus = 0
        self.defender_bonus = 0


class DeclaredAttackData:
    def __init__(self, defender_units: int):
        self.defender_units = defender_units


class DiceRolledData:
    def __init__(self, attacker_roll: list, defender_roll: list):
        self.attacker_roll = attacker_roll
        self.defender_roll = defender_roll


# Combat procedures ----------------------------------------

# Basic dice procedures-----
def roll(n_dice: int, n_bonus: int):
    normal_dices = [randint(1, 6) for x in range(n_dice)]
    combat_dices = [CombatDice(randint(0, 2)) for x in range(n_bonus)]
    return (normal_dices, combat_dices)


def group_dices(unsorted_dices: list):
    sorted_dice = sorted(unsorted_dices)
    dice_sum = 0
    n_groups = 0
    while sorted_dice:
        dice_sum += sorted_dice.pop() if dice_sum else sorted_dice.pop(0)
        if dice_sum >= 6:
            n_groups += 1
            dice_sum = 0
    return n_groups


def roll_dices(
    pre_combat: PreCombatData, declared_attack: DeclaredAttackData
) -> DiceRolledData:
    attacker_roll = roll(pre_combat.attacker_units, pre_combat.attacker_bonus)
    defender_roll = roll(declared_attack.defender_units, pre_combat.defender_bonus)
    return (attacker_roll, defender_roll)
