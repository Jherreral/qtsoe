# from pytestqt.qt_compat import qt_api
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialogButtonBox

from soe.client.element_dialog import ElementDialog


def test_element_dialog(qtbot):
    def checker(picked_elements):
        print(f"{picked_elements}")
        return ["Element2"] == picked_elements

    element_list = ["Element1", "Element2", "Element3", "Element4"]
    element_type = "String"
    to_pick = 1
    dialog = ElementDialog(element_list, element_type, to_pick)

    qtbot.addWidget(dialog)

    with qtbot.waitSignal(
        dialog.selected_elements, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        dialog.show()
        qtbot.mouseClick(dialog.element_buttons[1], Qt.LeftButton)
        qtbot.mouseClick(dialog.element_buttons[3], Qt.LeftButton)
        qtbot.mouseClick(dialog.button(QDialogButtonBox.Ok), Qt.LeftButton)
    assert blocker.signal_triggered
