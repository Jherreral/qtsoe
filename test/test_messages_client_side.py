from time import sleep

from pytestqt.qt_compat import qt_api

from PySide2.QtCore import Qt, QTimer
from PySide2.QtWidgets import QApplication, QDialogButtonBox

from soe.core.game_map import GameMap
from soe.core.game_components import Player

from soe.client import Client

# from soe.client.map_widget import MapWidget
from soe.core.message import (
    Message,
    PickSingleZone,
    PickAmount,
    # PickSingleZone,
    # PickManyZones,
    PickActionZones,
    PickName,
    PickElements,
    # CardNotValid,
    PlaceUnits,
    # PickedAmount,
    # PickedSingleZone,
    # PickedName,
    # PickedElement,
    # PlacedUnits,
    # PlayCard,
    UpdateBoard,
)


def test_basics(qtbot):
    """
    Basic test that works more like a sanity check to ensure we are setting up a
    QApplication properly and are able to display a simple event_recorder.
    """
    assert qt_api.QApplication.instance() is not None
    widget = qt_api.QWidget()
    qtbot.addWidget(widget)
    widget.setWindowTitle("W1")
    widget.show()

    assert widget.isVisible()
    assert widget.windowTitle() == "W1"


def test_pick_amount_processing(qtbot):
    """Test if PickAmount message is processed as expected."""
    client = Client()
    qtbot.addWidget(client)

    def handle_dialog():
        dialog = QApplication.activeWindow()
        dialog.setIntValue(3)
        print(f"Int set {dialog.intValue()}")
        dialog.accept()
        print("Accepted")

    def checker(message: Message):
        return message.value == 3

    # Inside SignalBlocker we call 'handle_dialog' and process_message:
    # 1. process_message should open up the dialog with range 2 to 5.
    # 2. The timer for handle_dialog goes off and tries to click the dialog.
    # 3. If the 2 previous steps work as expected, handle_dialog will trigger the final
    # part of process_message and emit a send_response signal with value 3.
    # 4. The SignalBlocker receives the emitted signal; if it has the right value the
    # signal_emitted attribute is set to True.
    # 5. The final assert will tell if everything went as expected.

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        QTimer.singleShot(500, handle_dialog)
        client.process_message(PickAmount("Test", 2, 5))
    assert blocker.signal_triggered


def test_pick_single_zone_processing(qtbot):
    """Test if PickSingleZone message is processed as expected.
    """

    def checker(message: Message):
        return message.zone == "Chile"

    zones = ["Chile", "Argen", "Brasi"]
    client = Client()
    qtbot.addWidget(client)
    mp = client.player_view.map_widget
    client.process_message(PickSingleZone("Test", zones))

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        qtbot.mouseClick(mp.zones["Chile"], Qt.LeftButton)
        assert mp._selected_zone_name == "Chile"
        qtbot.mouseClick(mp._confirm_button, Qt.LeftButton)
    assert blocker.signal_triggered


def test_pick_action_zones_processing(qtbot):
    """Test if PickActionZones message is processed as expected.
    """

    def checker(message: Message):
        return message.action_zones == ("Chile", "Argen")

    client = Client()
    qtbot.addWidget(client)
    mp = client.player_view.map_widget
    origins = ["Chile", "Argen", "Brasi"]
    destinations = GameMap.get_destinations(origins)
    action_zones = {origin: destinations[idx] for idx, origin in enumerate(origins)}
    client.process_message(PickActionZones("Test", action_zones))

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        qtbot.mouseClick(mp.zones["Chile"], Qt.LeftButton)
        qtbot.mouseClick(mp.zones["Argen"], Qt.LeftButton)
        assert mp._origin == "Chile"
        assert mp._destination == "Argen"
        qtbot.mouseClick(mp._confirm_button, Qt.LeftButton)
    assert blocker.signal_triggered


def test_pick_name_processing(qtbot):
    """Test if PickName message is processed as expected."""
    client = Client()
    qtbot.addWidget(client)

    def handle_dialog():
        dialog = QApplication.activeWindow()
        dialog.setTextValue("Pepito")
        dialog.accept()

    def checker(message: Message):
        return message.name == "Pepito"

    # Inside SignalBlocker we call 'handle_dialog' and process_message:
    # 1. process_message should open up the dialog.
    # 2. The timer for handle_dialog goes off and the funciton tries to set the dialog.
    # 3. If the 2 previous steps work as expected, handle_dialog will trigger the final
    # part of process_message and emit a send_response signal with a proper name.
    # 4. The SignalBlocker receives the emitted signal; if it has the right value the
    # signal_emitted attribute is set to True.
    # 5. The final assert will tell if everything went as expected.

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        QTimer.singleShot(500, handle_dialog)
        client.process_message(PickName("Test", 0))
    assert blocker.signal_triggered


def test_place_units_processing(qtbot):
    """Test if PlaceUnits message is processed as expected.
    """

    def checker(message: Message):
        expected = {
            ("Chile", 2),
            ("Brasi", 2),
            ("Argen", 1),
        }
        print(f"Checking {expected} and {message.zones_units}")
        return message.zones_units == expected

    client = Client()
    qtbot.addWidget(client)
    mp = client.player_view.map_widget
    zones = ["Chile", "Argen", "Brasi"]
    units = 5
    client.process_message(PlaceUnits("Test", zones, units))

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        assert mp._counter_widget.value == units
        qtbot.mouseClick(mp.zones["Chile"], Qt.LeftButton)
        qtbot.mouseClick(mp.zones["Chile"], Qt.LeftButton)
        qtbot.mouseClick(mp.zones["Brasi"], Qt.LeftButton)
        qtbot.mouseClick(mp.zones["Argen"], Qt.LeftButton)
        qtbot.mouseClick(mp.zones["Brasi"], Qt.LeftButton)
        assert mp._counter_widget.value == 0
        qtbot.mouseClick(mp._confirm_button, Qt.LeftButton)
    assert blocker.signal_triggered


def test_pick_elements_processing(qtbot):
    """Test if PickElements message is processed as expected.
    """
    element_list = ["Ele1", "Ele2", "Ele3"]
    element_type = "String"
    pick_amount = 1
    element_to_pick = 2

    def checker(message: Message):
        expected = [element_list[element_to_pick]]
        print(f"Checking {expected} and {message.elements}")
        return message.elements == expected

    client = Client()
    qtbot.addWidget(client)

    def handle_dialog():
        qtbot.mouseClick(dialog.element_buttons[element_to_pick], Qt.LeftButton)
        qtbot.mouseClick(dialog.button(QDialogButtonBox.Ok), Qt.LeftButton)

    client.process_message(
        PickElements("Test", element_list, element_type, pick_amount)
    )
    dialog = client.player_view._dialog

    with qtbot.waitSignal(
        client.send_response, timeout=5000, raising=False, check_params_cb=checker
    ) as blocker:
        QTimer.singleShot(500, handle_dialog)
    assert blocker.signal_triggered


def test_update_board_processing(qtbot):
    """Test if UpdateBoard message is processed as expected.
    """
    game_info = {
        "Chile": ("Teocracy", 3,),
        "Mexic": ("Democracy", 5,),
        "Brasi": ("Communism", 2,),
    }

    factions_in_play = ["Teocracy", "Democracy", "Communism"]

    game_map = GameMap()
    game_map.set_game_info(game_info)
    game_map.factions_in_play = factions_in_play

    cards_in_hand = ["Bla", "Ble", "Bli"]
    client = Client()
    qtbot.addWidget(client)
    client.player_view.track.set_factions_in_header(factions_in_play)

    client.process_message(
        UpdateBoard(
            "Test",
            {
                "map": game_map.get_map_info(),
                "track": game_map.get_track_info(),
                "hand": ("Dummy", cards_in_hand),
            },
        )
    )

    # Asserts for map widget
    for zone in game_info.keys():
        assert client.player_view.map_widget.zones[zone].faction == game_info[zone][0]
        assert client.player_view.map_widget.zones[zone].value == game_info[zone][1]

    # Change to True if you want to see the widget
    if True:
        client.show()
        qtbot.waitForWindowShown(client)
        sleep(5)

    # Asserts for track widget
    track = client.player_view.track
    for idx, faction in enumerate(factions_in_play):
        # Assert that the headers are in the right order
        assert track.horizontalHeaderItem(idx).text() == faction

    # Assert that the values in the track widget are right
    assert int(client.player_view.track.item(0, 0).text()) == 1  # Chile Prod
    assert int(client.player_view.track.item(0, 1).text()) == 3  # Mexic Prod
    assert int(client.player_view.track.item(0, 2).text()) == 4  # Brasi Prod
    assert int(client.player_view.track.item(1, 0).text()) == 0  # No Oil in Chile
    assert int(client.player_view.track.item(1, 1).text()) == 1  # Oil in Mexic
    assert int(client.player_view.track.item(1, 2).text()) == 1  # Oil in Brasi
    assert int(client.player_view.track.item(2, 0).text()) == 0  # No Sphere
    assert int(client.player_view.track.item(2, 1).text()) == 0  # No Sphere
    assert int(client.player_view.track.item(2, 2).text()) == 0  # No Sphere
    assert int(client.player_view.track.item(3, 0).text()) == 0  # No Cap Chile
    assert int(client.player_view.track.item(3, 1).text()) == 1  # Yes Cap Mexic
    assert int(client.player_view.track.item(3, 2).text()) == 1  # Yes Cap Brasi

    # Assert for hand widget
    for idx, card_name in enumerate(cards_in_hand):
        assert (
            client.player_view.hand._cards_layout.itemAt(idx).widget().text()
            == card_name
        )
