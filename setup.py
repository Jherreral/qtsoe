import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="soe",
    version="0.0.1",
    author="Jaime Herrera",
    author_email="j.herrera.lv@gmail.com",
    description="A digitalization of Spheres of Influence on Qt",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Jherreral/qtsoe",
    packages=['soe','soe/client','soe/core','soe/server'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=["PySide2", "pytest-qt", "more_itertools", "importlib_resources"],
    include_package_data=True
)
