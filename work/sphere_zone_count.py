from pathlib import Path


class Zone:
    def __init__(self):
        self.name = None
        self.sphere = None
        self.production = None
        self.oil = None
        self.compass = None
        self.capital = None
        self.value = None
        self.faction = None


def parseZones(zones, pathToAssets):
    zonesPath = pathToAssets / Path('SpheresDataBoard.csv')
    with open(zonesPath, 'r') as f:
        for line in f.readlines():
            data = line.split(';')
            keyName = data[1]
            zone = Zone()
            zone.id = int(data[0])
            zone.sphere = int(data[2])
            zone.production = int(data[3])
            zone.oil = int(data[4])
            zone.compass = bool(int(data[5]))
            zone.capital = bool(int(data[6]))
            zone.value = 0
            zones[keyName] = zone
        f.close()


def CalculateZonesPerSphere(zones, sphereCount):
    for z in zones:
        if zones[z].sphere == 0:
            continue
        sphereCount[zones[z].sphere] += 1


pathToAssets = Path(__file__).resolve().parent.parent / Path('assets')
zones = {}
parseZones(zones, pathToAssets)
sphereCount = {x: 0 for x in range(1, 19)}
CalculateZonesPerSphere(zones, sphereCount)
print(sphereCount)
