from PySide2.QtWidgets import (
    QApplication,
    QPushButton,
    QMainWindow,
    QInputDialog,
    QVBoxLayout,
    QWidget,
)
from PySide2.QtCore import Slot, QObject


class StateMachine:  # NO QT HERE
    def __init__(self, parent, get_sync_input_callback):
        self._state = 0
        self._parent = parent
        self._last_user_input = 0
        self._async_input = []
        self._sync_callback = get_sync_input_callback

<<<<<<< HEAD
    def state1_entry(self):
=======
    def state1(self):
>>>>>>> adf32fbff591d4ed443ce563df55f0dcb7a9d082
        self._state = 1
        print("In state 1")
        print(f"Last input: {self._last_user_input}")
        # Request an input from the user
        number = self._sync_callback()
        if number < 200:
            self._last_user_input = number
        print(self._async_input)

<<<<<<< HEAD
    def state2_entry(self):
=======
    def state2(self):
>>>>>>> adf32fbff591d4ed443ce563df55f0dcb7a9d082
        self._state = 2
        print("In state 2")
        print(f"Last input: {self._last_user_input}")
        # Request an input from the user
        number = self._sync_callback()
        if number < 200:
            self._last_user_input = number
        print(self._async_input)

<<<<<<< HEAD
    def state3_entry(self):
=======
    def state3(self):
>>>>>>> adf32fbff591d4ed443ce563df55f0dcb7a9d082
        self._state = 3
        print("In state 3")
        print(f"Last input: {self._last_user_input}")
        # Request an input from the user
        number = self._sync_callback()
        if number < 200:
            self._last_user_input = number
        print(self._async_input)

    def next(self):
        if self._state == 1:
<<<<<<< HEAD
            self.state2_entry()
        elif self._state == 2:
            self.state3_entry()
        elif self._state == 3:
            self.state1_entry()
        else:
            self.state1_entry()
=======
            self.state2()
        elif self._state == 2:
            self.state3()
        elif self._state == 3:
            self.state1()
        else:
            self.state1()
>>>>>>> adf32fbff591d4ed443ce563df55f0dcb7a9d082


class Server(QObject):
    @Slot()
    def get_sync_input(self):
        number, ok = QInputDialog.getInt(
            self.window, "Input Number Dialog", "Enter number", 0
        )
        if ok:
            return number
        else:
            return 0

    @Slot()
    def next_state(self):
        self.state_machine.next()

    @Slot()
    def send_async_input(self):
        text, ok = QInputDialog.getText(self.window, "Input Text Dialog", "Enter text")
        if ok:
            self.state_machine._async_input.append(text)

    def __init__(self):
        app = QApplication([])
        self.window = QMainWindow()
        center_widget = QWidget()
        self.window.setCentralWidget(center_widget)
        layout = QVBoxLayout(center_widget)
        advance_btn = QPushButton("Next state")
        advance_btn.clicked.connect(self.next_state)
        async_input_btn = QPushButton("Click me to send async input")
        async_input_btn.clicked.connect(self.send_async_input)
        layout.addWidget(advance_btn)
        layout.addWidget(async_input_btn)

        self.state_machine = StateMachine(self.window, self.get_sync_input)

        self.window.show()
        app.exec_()


server = Server()
