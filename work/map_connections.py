from pathlib import Path
import json


def parseEdges(mapGraph, pathToAssets):
    edgesPath = pathToAssets / Path('IncidenceT2.txt')
    with open(edgesPath, 'r') as f:
        for line in f.readlines():
            nodeA = int(line.find('1'))
            nodeB = int(line.find('1', nodeA+1))
            mapGraph.append((nodeA, nodeB))
        f.close()


def getConnectionsToZone(mapGraph, n):
    pairs1 = [p[1] for p in mapGraph if p[0] == n]
    pairs2 = [p[0] for p in mapGraph if p[1] == n]
    return pairs1+pairs2


pathToAssets = Path(__file__).resolve().parent.parent / Path('assets')
mapGraph = []
parseEdges(mapGraph, pathToAssets)
connections = {}
for n in range(100):
    connections[n] = getConnectionsToZone(mapGraph, n)

names = [
    'Argen',
    'Brasi',
    'Chile',
    'Colom',
    'Venez',
    'Panam',
    'Cuba0',
    'Mexic',
    'Calif',
    'Ohio0',
    'Texas',
    'Miami',
    'NewYk',
    'Vanco',
    'Montr',
    'Alask',
    'NtCan',
    'Islan',
    'Ingla',
    'Finla',
    'Sueci',
    'Alema',
    'Franc',
    'Espan',
    'Autri',
    'Itali',
    'Polon',
    'Ucran',
    'Balca',
    'Turqu',
    'Marru',
    'Alger',
    'Libia',
    'Egipt',
    'Mali0',
    'Niger',
    'Congo',
    'Etiop',
    'Mozam',
    'Namib',
    'Sudaf',
    'Madag',
    'Israe',
    'Siria',
    'Iraq0',
    'Saudi',
    'Iran0',
    'Afgan',
    'Turkm',
    'Uzbek',
    'Kazak',
    'Moscu',
    'Bielo',
    'Novgo',
    'Siber',
    'Kamch',
    'Jilin',
    'NCore',
    'Tibet',
    'Beiji',
    'SCore',
    'Hongk',
    'Pakis',
    'India',
    'Bangl',
    'Myanm',
    'Taila',
    'Japon',
    'Taiwn',
    'Filip',
    'Indon',
    'Papua',
    'Ausst',
    'Sydne',
    'Zelan',
    'M_PacificoArtico',
    'M_PacificoNorte',
    'M_PacificoEcuatorial',
    'M_CaribeSur',
    'M_GolfoMexico',
    'M_AtlanticoNorte',
    'M_AtlanticoGalo',
    'M_AtlanticoArtico',
    'M_delNorte',
    'M_Gibraltar',
    'M_AtlanticoCentro',
    'M_AtlanticoSur',
    'M_PacificoSur',
    'M_Sudafrica',
    'M_IndicoOeste',
    'M_Arabigo',
    'M_IndicoNorte',
    'M_Chipre',
    'M_TirrenoJonico',
    'M_deFilipinas',
    'M_ChinaMerid',
    'M_deArafura',
    'M_delCoral',
    'M_IndicoEste',
    'M_GranBahiaAustr',
]

namesPath = pathToAssets / Path('SpheresDataBoard.csv')
rightNames = []
with open(namesPath, 'r') as f:
    for line in f.readlines():
        data = line.split(';')
        rightNames.append(data[1])


namedConnections = {rightNames[id]: [rightNames[c]for c in connections[id]]
                    for id in connections}
print(namedConnections)

resultPath = pathToAssets / Path('mapConnections.csv')
jsonPath = pathToAssets / Path('mapConnections.json')

with open(jsonPath, 'w') as r:
    json.dump(namedConnections, r)
