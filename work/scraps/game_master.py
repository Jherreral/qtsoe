import logging

from PySide2.QtWidgets import (
    QInputDialog
)
from PySide2.QtCore import Slot, Signal, QObject

from gameboard import (
    GameBoard,
    Player
)


class GameMaster(QObject):
    """
    Manages the game board and the flow of the game.
    Has the initializeBoard() to setup the board,
    play() to start or resume the game.
    """

    _log = logging.getLogger('PlayerViewLog')
    playersCreated = Signal(list)
    setBoard = Signal(list)
    playerMapActionNeeded = Signal(str, set)

    def __init__(self, parent=None):
        super(GameMaster, self).__init__(parent)
        self.gb = GameBoard()
        self.setup = True
        self.phase = 1

    def play(self):
        pass

    @Slot()
    def nextState(self):
        if self.setup:
            self.initialSetUp()
            self.setup = False
        else:
            if self.phase == 0:
                self.startMobilizationPhase()
            elif self.phase == 1:
                self.startTurn()

    def initialSetUp(self):
        '''Ask for player amount, set names, factions and capitals.'''
        # Get number of players.
        number, ok = QInputDialog.getInt(
            self.parent(),
            "Player selector",
            "Enter amount of players:",
            2)
        if not ok or number < 2 or number > 8:
            raise RuntimeWarning('Number of players must be between 2 and 8')
        self.createPlayers(number)
        self.setInitialForces()
        self.gb.prepareTurnDeck(True)

    def createPlayers(self, n):
        for i in range(n):
            self.gb.players.append(Player())

        capitalPairs = self.gb.getNCapitalPairs(n)
        availableFactions = self.gb.factions.copy()
        availableNames = [f.name for f in self.gb.factions]
        for i, pc in enumerate(zip(self.gb.players, capitalPairs)):
            p, caps = pc
            p.user = i + 1
            name, ok = QInputDialog.getText(self.parent(),
                                            f'Player creation ({p.user})',
                                            f'Insert name:',
                                            text=f'Player_{p.user}')
            if name and ok:
                p.name = name
            faction, ok = QInputDialog.getItem(
                            self.parent(),
                            f'Player creation ({p.user})',
                            "Choose faction",
                            availableNames)
            if faction and ok:
                selectedIndex = availableNames.index(faction)
                p.faction = availableFactions[selectedIndex]
                availableNames.pop(selectedIndex)
                availableFactions.pop(selectedIndex)

            cap, ok = QInputDialog.getItem(
                self.parent(),
                f'Player creation ({p.user})',
                "Choose capital",
                list(caps))
            if cap and ok:
                p.capital = cap
            self._log.info(f'Player {p.name}, {p.faction.name},'
                           f'with capital {p.capital}, created.')
            print(f'Player {p.name}, {p.faction.name},'
                  f'with capital {p.capital}, created.')
        self.playersCreated.emit(self.gb.players)

    def setInitialForces(self):
        forces = [(p.capital, 3, p.faction) for p in self.gb.players]
        self.gb.setBoardZoneValues(forces)

    def startTurn(self):
        '''Check cards and ask the current player for action.'''
        # _checkCards = False
        currentPlayer = self.gb.turnDeck.pop()
        self._log.info(
            f'Starting {currentPlayer}'
            f'({self.gb.getPlayer_N(currentPlayer).faction.name}) turn.')
        # Tell the map to activate the apropriate zones for the current player.
        mapZones = self.computeMapForPlayerAction(currentPlayer)
        self.playerMapActionNeeded.emit(currentPlayer, mapZones)

    def computeMapForPlayerAction(self, name):
        '''Get the list of available interactive zones for the player.'''
        playerZones = self.gb.getPlayerZones(name)
        mapZones = set()
        for z in playerZones:
            connections = self.gb.getConnectionsToZone(z)
            for c in connections:
                mapZones.add(c)
        return mapZones
