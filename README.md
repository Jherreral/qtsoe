# qtsoe

This project is a digitalization of the board game "Spheres of Influence" by Little Nuke Games, which started as an opportunity to learn about Python and its use in desktop applications. The end goal is to have a game that can be played both locally and over a network, but most importantly that is FUN to play.
Why SOE instead of SOI? Because I made a typo when creating the project ¯\_(ツ)_/¯

# Get sources

Use git clone.

# Build

cd to the root directory, then use `python setup.py bdist_wheel`

# Test

cd to the root directory, the use `python -m pytest`. Pytest discovers the tests in 'test' folder automatically.

# Install

Create a virtualenv, activate it and pip-install the wheel located in `qtsoe/dist/soe_xxxxxxxx.whl` 

# Run

Use `python -m soe`
