## 2020-07-04:

Finished implementing the handling of PickSingleZone and PlaceUnits in the MapWidget, plus with cleaning the widgets to process future messages.

## 2020-07-05:

Realized that my design for action messages was too 'chatty': I was sending a message to the client for him to pick 1 zone as the origin, receive the response and send another PickSingleZone for the destination. Instead, it seems better know to add a new message called PickActionZones to let the client deal with the whole operation.
Having second thoughts about the 'static' programming of GameMap. Will it comeback and bite me? The reason was to provide certain 'read-only' methods without having to import the gameboard instance with its gamemap instance.

## 2020-07-11:

I need to add a new state (and color) to the Game Map widget, as the origin and destination zones for turn actions need to be different.

## 2020-07-14:

I need even more colors, the current ones are not intuitive. Also, another state is missing: unselected_but_origin_picked. It is horrible, so next commit will probably be a rename of the states...
Or not, because I started clearing up the pixmap hassle: now there are ZoneStates that hold both hover/unhover pixmaps.
Next task, replace the public 'state' booleans in ZoneWidget with a single state variable (most probably a Enum). Hopefully the states are all mutually exclusive, if not I will have to find another solution.

## 2020-07-16

Refactored map_widget and zone_widget to use the new enum ZoneState. It looks way clearer now.
Next step would be refactoring message.py and client.py to avoid type checks and hard-named functions like MapWidget.process_pickSingleZone_message.

## 2020-07-17

Installed both Gitlab Workflow and pytest-qt. With the former I will learn new Git trick, and the latter already allowed me to make my first GUI test. Yay!

## 2020-07-25

Next step: check that messages are well sent and well received.
Added a cool test that sets the value on the QInputDialog created by Client.process_message() and checks if the response is emitted properly. Awesome!

## 2020-07-26

Added more tests for the rest of the implemented messages, though some have to be extended to test the whole client side processing. I added more issues in Gitlab and a new mileston to gather all project-related issues. Also I'm testing time-tracking per commit.
I extended the tests' coverage to include the client receive/send message code.
I implemented PickElements message with strings as elements.
The client stuff should be enough now, next step will be to implement the game board and its message to the client. That will allow some neat tests!

## 2020-08-01

New issues in Gitlab to track the Core module.
Started working on UpdateBoard message and the helper function to get data from the internal GameBoard to the client interface.

## 2020-08-07

Finally finished the helper functions for the Track widget. Now the UpdateBoard message can carry map and track info from the Core to the Client.

## 2020-08-10

What to do, what to do? At this point every feature will be take some time to implement; better to be clear about the next steps.
Well, let's try implementing the structures for cards.

## 2020-08-12

I implemented some hand and card widgets, but I will feel safer with a test in place. Next time I'll add it.

## 2020-08-18

Made the test for UpdateBoard with map, track and hand info, and improved the hand widget. The latter needed some love, as it was configured as in the previous iteration of this application.

## 2020-08-19

Started writing game logic, it seems like the best way to put some pressure and see where everything falls apart.
